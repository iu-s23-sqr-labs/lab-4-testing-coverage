package com.hw.db.controllers;


import com.hw.db.models.User;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;


class userDAOTests {
    JdbcTemplate db;
    UserDAO userDao;

    @BeforeEach
    void setUp() {
        this.db = Mockito.mock(JdbcTemplate.class);
        this.userDao = new UserDAO(this.db);
    }

    @NotNull
    static List<Arguments> arguments() {
        return List.of(
            Arguments.of(new User("username", null, null, "about"), "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),

            Arguments.of(new User("username", null, "fullname", null), "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
            Arguments.of(new User("username", null, "fullname", "about"), "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),

            Arguments.of(new User("username", "email", null, null), "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
            Arguments.of(new User("username", "email", null, "about"), "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
            Arguments.of(new User("username", "email", "fullname", null), "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
            Arguments.of(new User("username", "email", "fullname", "about"), "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;")
        );
    }

    @Test
    void testChange1() {
        UserDAO.Change(new User("username", null, null, null));
        Mockito.verify(this.db, Mockito.never()).update(Mockito.anyString(), Optional.ofNullable(Mockito.any()));
    }

    @ParameterizedTest
    @MethodSource("arguments")
    void testChange2(User user, String query) {
        UserDAO.Change(user);
        Mockito.verify(this.db).update(Mockito.eq(query), Optional.ofNullable(Mockito.any()));
    }
}
