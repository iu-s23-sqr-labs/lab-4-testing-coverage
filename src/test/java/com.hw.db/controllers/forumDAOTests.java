package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.validation.constraints.NotNull;
import java.util.List;

class forumDAOTests {
    JdbcTemplate db;
    ForumDAO forumDao;

    @BeforeEach
    void setUp() {
        this.db = Mockito.mock(JdbcTemplate.class);
        this.forumDao = new ForumDAO(this.db);
    }

    @NotNull
    static List<Arguments> arguments() {
        return List.of(
                Arguments.of("", 5, "", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Arguments.of("", null, null, false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Arguments.of("", null, "", false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;")
        );
    }

    @ParameterizedTest
    @MethodSource("arguments")
    void testUserList(String slug, Integer limit, String since, Boolean isDescendingOrder, String expectedQuery) {
        ForumDAO.UserList(slug, limit, since, isDescendingOrder);
        Mockito.verify(this.db).query(Mockito.eq(expectedQuery), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}
