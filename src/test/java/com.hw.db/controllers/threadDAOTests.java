package com.hw.db.controllers;


import com.hw.db.models.User;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.PostDAO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;


class threadDAOTests {
    JdbcTemplate db;
    ThreadDAO threadDao;

    @BeforeEach
    void setUp() {
        this.db = Mockito.mock(JdbcTemplate.class);
        this.threadDao = new ThreadDAO(this.db);
    }

    @NotNull
    static List<Arguments> arguments() {
        return List.of(
            Arguments.of(1, 2, 3, false, "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
            Arguments.of(1, null, 3, true, "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC ;")
        );
    }


    @ParameterizedTest
    @MethodSource("arguments")
    void testTreeSort(Integer id, Integer limit, Integer since, Boolean is_desc, String query) {
        ThreadDAO.treeSort(id, limit, since, is_desc);
        Mockito.verify(this.db).query(Mockito.eq(query), Mockito.any(PostDAO.PostMapper.class), Mockito.any());
    }
}
