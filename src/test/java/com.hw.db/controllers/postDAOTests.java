package com.hw.db.controllers;


import com.hw.db.models.User;
import com.hw.db.models.Post;
import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.PostDAO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.sql.Timestamp;
import java.util.Optional;


class postDAOTests {
    JdbcTemplate db;
    PostDAO postDao;

    @BeforeEach
    void setUp() {
        this.db = Mockito.mock(JdbcTemplate.class);
        this.postDao = new PostDAO(this.db);
        Mockito.when(db.queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1))).thenReturn(new Post("author", new Timestamp(1), "forum", "message", null, null, false));
    }

    @NotNull
    static List<Arguments> arguments() {
        return List.of(
            Arguments.of(new Post("author", new Timestamp(2), "forum", "message", null, null, false), "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Arguments.of(new Post("author", new Timestamp(2), "forum", "message2", null, null, false), "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Arguments.of(new Post("author2", new Timestamp(1), "forum", "message", null, null, false), "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
            Arguments.of(new Post("author2", new Timestamp(2), "forum", "message2", null, null, false), "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;")
        );
    }

    @Test
    void testSetPost1() {
        PostDAO.setPost(1, new Post("author", new Timestamp(1), "forum", "message", null, null, false));
        Mockito.verify(this.db, Mockito.never()).update(Mockito.any(), Optional.ofNullable(Mockito.any()));
    }

    @ParameterizedTest
    @MethodSource("arguments")
    void testSetPost2(Post post, String query) {
        PostDAO.setPost(1, post);
        Mockito.verify(this.db).update(Mockito.eq(query), Optional.ofNullable(Mockito.any()));
    }
}
